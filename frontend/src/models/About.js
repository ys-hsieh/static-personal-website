import portrait from '../assets/portrait.png';
import resume from '../assets/Yuan-Shen Hsieh.pdf';
import '../assets/about.css';

function About() {
    return (
        <div className="container grid about">
            <img src={portrait} id="portrait" alt="A Polaroid selfie of Yuan-Shen"/>
            <h1 id="title-txt"> &#128075; &nbsp; Hi there, I'm Yuan-Shen</h1>
            <p id="intro-txt">I'm a Master's student studying Computer Science at the <a href="https://dcarea.vt.edu/" target="_blank" rel="noreferrer">National Capital Region of Virginia Tech</a>. I have been working as a software engineer for around two years and will start my internship at Amazon this summer. Besides computer science, I also enjoy shooting films, writing poems and running. I set up this site not only because it feels cool to build one by myself but also because I can explore different possibilities with the Raspberry Pi underneath my table. Though it might seem a bit empty at this point, I hope this could motivate me to share more content, shoot more films, and document some of my experiments and adventures. If you have any exciting ideas to share, please feel free to context me through <a href="mailto:contact@yshsieh.link">email</a>. To know more about me, please visit my <a href="https://www.linkedin.com/in/ys-hsieh/" target="_blank" rel="noreferrer">LinkedIn</a> or take a look at my <a href={resume} target="_blank" rel="noreferrer">resume</a>.</p>
        </div>
    );
}

export default About;