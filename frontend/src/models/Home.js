import ZoomableSunburst from '../components/ZoomableSunburst';
import data from '../assets/intro.json';
import '../assets/home.css';

function Home() {
    return (
        <div className="container">
            {/* <RadialDzendrogram
                data={data}
                dimensions={{ width: 900, height: 960, margin: { top: 16, right: 16, bottom: 16, left: 16 } }}
            /> */}
            <ZoomableSunburst
                data={data}
                dimensions={{ width: 300, height: 300, margin: { top: 0, right: 0, bottom: 0, left: 0 } }}
            />
            <div className="flex-row idea-txt">
                <p>Design inspired by</p>
                <a href="https://chaidarun.com/" target="_blank" rel="noreferrer">Art Chaidarun</a>
            </div>
        </div>
    );
}

export default Home;