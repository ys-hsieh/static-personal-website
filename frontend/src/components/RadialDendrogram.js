import React from "react";
import * as d3 from "d3";

const RadialDendrogram = ({ data, dimensions }) => {
    const svgRef = React.useRef(null);
    const { width, height, margin } = dimensions;
    const svgWidth = width + margin.left + margin.right;
    const svgHeight = height + margin.top + margin.bottom;

    React.useEffect(() => {
        const radius = width / 2;
        const tree = d3.cluster().size([2 * Math.PI, radius - 100]);
        const root = tree(d3.hierarchy(data)
            .sort((a, b) => d3.ascending(a.data.name, b.data.name)));

        const svg = d3.select(svgRef.current);

        svg.append("g")
            .attr("fill", "none")
            .attr("stroke", "#555")
            .attr("stroke-opacity", 0.6)
            .attr("stroke-width", 3)
            .selectAll("path")
            .data(root.links())
            .join("path")
            .attr("d", d3.linkRadial()
                .angle(d => d.x)
                .radius(d => d.y));

        svg.append("g")
            .selectAll("circle")
            .data(root.descendants())
            .join("circle")
            .attr("transform", d => `
              rotate(${d.x * 180 / Math.PI - 90})
              translate(${d.y},0)
            `)
            .attr("fill", d => d.children ? "#555" : "#999")
            .attr("r", 2.5);

        svg.append("g")
            .attr("font-family", "sans-serif")
            .attr("font-size", 10)
            .attr("stroke-linejoin", "round")
            .attr("stroke-width", 5)
            .selectAll("text")
            .data(root.descendants())
            .join("text")
            .attr("transform", d => `
              rotate(${d.x * 180 / Math.PI - 90}) 
              translate(${d.y},0) 
              rotate(${d.x >= Math.PI ? 180 : 0})
            `)
            .attr("dy", "0.31em")
            .attr("x", d => d.x < Math.PI === !d.children ? 6 : -6)
            .attr("text-anchor", d => d.x < Math.PI === !d.children ? "start" : "end")
            .text(d => d.data.name)
            .clone(true).lower()
            .attr("stroke", "white");

        svg.attr("viewBox", () => {
            return [-svgWidth / 2, -svgHeight / 2, svgWidth, svgHeight];
        });
    }, [data]); // Redraw chart if data changes

    // return <svg ref={svgRef} width={svgWidth} height={svgHeight} />;
    return <svg ref={svgRef} id="radial-dendrogram-img"/>;
};

export default RadialDendrogram;