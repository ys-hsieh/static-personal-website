import React, { useState } from 'react';
import { Link } from "react-router-dom";
import logo from "../assets/logo.png"

function Header() {
    const [pathname, setPathname] = useState(window.location.pathname);

    return (
        <div className="container nav-bar">
            <Link
                to="/"
                onClick={() => setPathname('/')}
            >
                <img src={logo} alt="ysh Logo" id="logo-img"></img>
            </Link>
            <Link
                className={pathname === '/photo' ? "selected-nav-btn" : "nav-btn"}
                to="/photo"
                onClick={() => setPathname('/photo')}
            >
                Photo
            </Link>
            <Link
                className={pathname === '/blog' ? "selected-nav-btn" : "nav-btn"}
                to="/blog"
                onClick={() => setPathname('/blog')}
            >
                Blog
            </Link>
            <Link
                className={pathname === '/project' ? "selected-nav-btn" : "nav-btn"}
                to="/project"
                onClick={() => setPathname('/project')}
            >
                Project
            </Link>
            <Link
                className={pathname === '/about' ? "selected-nav-btn" : "nav-btn"}
                to="/about"
                onClick={() => setPathname('/about')}
            >
                About
            </Link>
        </div>
    );
}

export default Header;